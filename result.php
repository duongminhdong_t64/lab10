<!DOCTYPE html>
<!--code by webdevtrick (webdevtrick.com) -->
<html>

<head>
    <meta charset="UTF-8" />
    <style>
        #specifyColorRed {
            accent-color: red;
            color: red;
        }

        #specifyColorGreen {
            accent-color: green;
            color: green;
        }

        .radio-custom:checked~label {
            color: blue;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
    <script>

    </script>
    <div id="page-wrap">

        <h1>Kết Quả bạn đạt được :</h1>

        <?php
        // Mảng kết quả 
        $answers = array('B', 'D', 'C', 'A', 'D', 'C', 'D', 'C', 'C', 'B');
        // Mảng kết quả lựa chọn
        $answerSelect = array();
        session_start();
        $answerSelect[] = $_SESSION['question-1-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-2-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-3-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-4-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-5-answers'] ?? "";
        $answerSelect[] = $_POST['question-6-answers'] ?? "";
        $answerSelect[] = $_POST['question-7-answers'] ?? "";
        $answerSelect[] = $_POST['question-8-answers'] ?? "";
        $answerSelect[] = $_POST['question-9-answers'] ?? "";
        $answerSelect[] = $_POST['question-10-answers'] ?? "";

        $totalCorrect = 0;
        $comment = "";

        for ($i = 0; $i < count($answers); $i++) {
            if ($answerSelect[$i] == $answers[$i]) {
                $totalCorrect++;
            }
        }

        echo "<div id='results'>$totalCorrect / 10 câu đúng</div>";
        if ($totalCorrect < 4) {
            echo "<div id='results'>Bạn quá kém, bạn cần ôn tập thêm !</div>";
        } elseif ($totalCorrect >= 4 && $totalCorrect <= 7) {
            echo "<div id='results'> Cũng bình thường thôi hehe !  </div>";
        } else {
            echo "<div id='results'> Sắp sửa làm được trợ giảng lớp PHP rồi đó !! </div>";
        }

        ?>


    </div>
    <ol>

        <li>

            <h3>Câu 1: Con gì có bốn chân kêu meo meo</h3>
            <div>
                <input id="<?php echo ($answerSelect[0] == 'A' & $answers[0] == 'A') ? 'specifyColorGreen' : (($answerSelect[0] == 'A') ? 'specifyColorRed' : (($answers[0] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-1-answers" value="A" <?php echo ($answerSelect[0] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[0] == 'A' & $answers[0] == 'A') ? 'specifyColorGreen' : (($answerSelect[0] == 'A') ? 'specifyColorRed' : (($answers[0] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-1-answers-A">A) Con chó </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[0] == 'B' & $answers[0] == 'B') ? 'specifyColorGreen' : (($answerSelect[0] == 'B') ? 'specifyColorRed' : (($answers[0] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-1-answers" value="B" <?php echo ($answerSelect[0] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[0] == 'B' & $answers[0] == 'B') ? 'specifyColorGreen' : (($answerSelect[0] == 'B') ? 'specifyColorRed' : (($answers[0] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-1-answers-B">B) Con Mèo</label>
            </div>

            <div>
                <input type="radio" name="question-1-answers" id="<?php echo ($answerSelect[0] == 'C' & $answers[0] == 'C') ? 'specifyColorGreen' : (($answerSelect[0] == 'C') ? 'specifyColorRed' : (($answers[0] == 'C') ? "specifyColorGreen" : "")) ?>" value="C" <?php echo ($answerSelect[0] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[0] == 'C' & $answers[0] == 'C') ? 'specifyColorGreen' : (($answerSelect[0] == 'C') ? 'specifyColorRed' : (($answer[0] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-1-answers-C">C) Con lợn</label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[0] == 'D' & $answers[0] == 'D') ? 'specifyColorGreen' : (($answerSelect[0] == 'D') ? 'specifyColorRed' : (($answers[0] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-1-answers" value="D" <?php echo ($answerSelect[0] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[0] == 'D' & $answers[0] == 'D') ? 'specifyColorGreen' : (($answerSelect[0] == 'D') ? 'specifyColorRed' : (($answers[0] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-1-answers-D">D) Con bò</label>
            </div>

        </li>

        <li>

            <h3>Câu 2: Trong nhà có bốn người Hoa yêu ai nhất</h3>

            <div>
                <input id="<?php echo ($answerSelect[1] == 'A' & $answers[1] == 'A') ? 'specifyColorGreen' : (($answerSelect[1] == 'A') ? 'specifyColorRed' : (($answers[1] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-2-answers" value="A" <?php echo ($answerSelect[1] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[1] == 'A' & $answers[1] == 'A') ? 'specifyColorGreen' : (($answerSelect[1] == 'A') ? 'specifyColorRed' : (($answers[1] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-2-answers-A">A) Ông hàng xóm</label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[1] == 'B' & $answers[1] == 'B') ? 'specifyColorGreen' : (($answerSelect[1] == 'B') ? 'specifyColorRed' : (($answers[1] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-2-answers" value="B" <?php echo ($answerSelect[1] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[1] == 'B' & $answers[1] == 'B') ? 'specifyColorGreen' : (($answerSelect[1] == 'B') ? 'specifyColorRed' : (($answers[1] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-2-answers-B">B) Chú bảo vệ </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[1] == 'C' & $answers[1] == 'C') ? 'specifyColorGreen' : (($answerSelect[1] == 'C') ? 'specifyColorRed' : (($answers[1] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-2-answers" value="C" <?php echo ($answerSelect[1] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[1] == 'C' & $answers[1] == 'C') ? 'specifyColorGreen' : (($answerSelect[1] == 'C') ? 'specifyColorRed' : (($answers[1] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-2-answers-C">C) Anh bộ đội</label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[1] == 'D' & $answers[1] == 'D') ? 'specifyColorGreen' : (($answerSelect[1] == 'D') ? 'specifyColorRed' : (($answers[1] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-2-answers" value="D" <?php echo ($answerSelect[1] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[1] == 'D' & $answers[1] == 'D') ? 'specifyColorGreen' : (($answerSelect[1] == 'D') ? 'specifyColorRed' : (($answers[1] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-2-answers-D">D) Bà nội</label>
            </div>

        </li>

        <li>

            <h3>Câu 3: "Hạt gạo làng ta" là ca khúc do ai sáng tác</h3>

            <div>
                <input id="<?php echo ($answerSelect[2] == 'A' & $answers[2] == 'A') ? 'specifyColorGreen' : (($answerSelect[2] == 'A') ? 'specifyColorRed' : (($answers[2] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-3-answers" value="A" <?php echo ($answerSelect[2] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[2] == 'A' & $answers[2] == 'A') ? 'specifyColorGreen' : (($answerSelect[2] == 'A') ? 'specifyColorRed' : (($answers[2] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-3-answers-A">A) Mạc Đĩnh Chi</label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[2] == 'B' & $answers[2] == 'B') ? 'specifyColorGreen' : (($answerSelect[2] == 'B') ? 'specifyColorRed' : (($answers[2] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-3-answers" value="B" <?php echo ($answerSelect[2] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[2] == 'B' & $answers[2] == 'B') ? 'specifyColorGreen' : (($answerSelect[2] == 'B') ? 'specifyColorRed' : (($answers[2] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-3-answers-B">B) Tố Hữu </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[2] == 'C' & $answers[2] == 'C') ? 'specifyColorGreen' : (($answerSelect[2] == 'C') ? 'specifyColorRed' : (($answers[2] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-3-answers" value="C" <?php echo ($answerSelect[2] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[2] == 'C' & $answers[2] == 'C') ? 'specifyColorGreen' : (($answerSelect[2] == 'C') ? 'specifyColorRed' : (($answers[2] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-3-answers-C">C) Trần Đăng Khoa </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[2] == 'D' & $answers[2] == 'D') ? 'specifyColorGreen' : (($answerSelect[2] == 'D') ? 'specifyColorRed' : (($answers[2] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-3-answers" value="D" <?php echo ($answerSelect[2] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[2] == 'D' & $answers[2] == 'D') ? 'specifyColorGreen' : (($answerSelect[2] == 'D') ? 'specifyColorRed' : (($answers[2] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-3-answers-D">D) Hàn Mạc Tử </label>
            </div>

        </li>

        <li>

            <h3>Câu 4: TotanHam là đội bóng của nước nào ?</h3>

            <div>
                <input id="<?php echo ($answerSelect[3] == 'A' & $answers[3] == 'A') ? 'specifyColorGreen' : (($answerSelect[3] == 'A') ? 'specifyColorRed' : (($answers[3] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-4-answers" value="A" <?php echo ($answerSelect[3] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[3] == 'A' & $answers[3] == 'A') ? 'specifyColorGreen' : (($answerSelect[3] == 'A') ? 'specifyColorRed' : (($answers[3] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-4-answers-A">A) Pháp </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[3] == 'B' & $answers[3] == 'B') ? 'specifyColorGreen' : (($answerSelect[3] == 'B') ? 'specifyColorRed' : (($answers[3] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-4-answers" value="B" <?php echo ($answerSelect[3] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[3] == 'B' & $answers[3] == 'B') ? 'specifyColorGreen' : (($answerSelect[3] == 'B') ? 'specifyColorRed' : (($answers[3] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-4-answers-B">B) Ý</label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[3] == 'C' & $answers[3] == 'C') ? 'specifyColorGreen' : (($answerSelect[3] == 'C') ? 'specifyColorRed' : (($answers[3] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-4-answers" value="C" <?php echo ($answerSelect[3] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[3] == 'C' & $answers[3] == 'C') ? 'specifyColorGreen' : (($answerSelect[3] == 'C') ? 'specifyColorRed' : (($answers[3] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-4-answers-C">C) Nhật</label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[3] == 'D' & $answers[3] == 'D') ? 'specifyColorGreen' : (($answerSelect[3] == 'D') ? 'specifyColorRed' : (($answers[3] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-4-answers" value="D" <?php echo ($answerSelect[3] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[3] == 'D' & $answers[3] == 'D') ? 'specifyColorGreen' : (($answerSelect[3] == 'D') ? 'specifyColorRed' : (($answers[3] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-4-answers-D">D) Pakistan </label>
            </div>

        </li>

        <li>

            <h3>Câu 5: Chiến tranh giữa Nga và quốc gia nào đang xảy ra hiện nay ?</h3>

            <div>
                <input id="<?php echo ($answerSelect[4] == 'A' & $answers[4] == 'A') ? 'specifyColorGreen' : (($answerSelect[4] == 'A') ? 'specifyColorRed' : (($answers[4] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-5-answers" value="A" <?php echo ($answerSelect[4] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[4] == 'A' & $answers[4] == 'A') ? 'specifyColorGreen' : (($answerSelect[4] == 'A') ? 'specifyColorRed' : (($answers[4] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-5-answers-A">A) Mỹ </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[4] == 'B' & $answers[4] == 'B') ? 'specifyColorGreen' : (($answerSelect[4] == 'B') ? 'specifyColorRed' : (($answers[4] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-5-answers" value="B" <?php echo ($answerSelect[4] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[4] == 'B' & $answers[4] == 'B') ? 'specifyColorGreen' : (($answerSelect[4] == 'B') ? 'specifyColorRed' : (($answers[4] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-5-answers-B">B) Nhật </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[4] == 'C' & $answers[4] == 'C') ? 'specifyColorGreen' : (($answerSelect[4] == 'C') ? 'specifyColorRed' : (($answers[4] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-5-answers" value="C" <?php echo ($answerSelect[4] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[4] == 'C' & $answers[4] == 'C') ? 'specifyColorGreen' : (($answerSelect[4] == 'C') ? 'specifyColorRed' : (($answers[4] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-5-answers-C">C) Anh </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[4] == 'D' & $answers[4] == 'D') ? 'specifyColorGreen' : (($answerSelect[4] == 'D') ? 'specifyColorRed' : (($answers[4] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-5-answers" value="D" <?php echo ($answerSelect[4] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[4] == 'D' & $answers[4] == 'D') ? 'specifyColorGreen' : (($answerSelect[4] == 'D') ? 'specifyColorRed' : (($answers[4] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-5-answers-D">D) Ukraina </label>
            </div>

        </li>
        <li>

            <h3>Câu 6: Bình phương 2 cạnh góc vuông bằng cạnh huyền trong tam giác vuông là định lý nào sau đây ?</h3>

            <div>
                <input id="<?php echo ($answerSelect[5] == 'A' & $answers[5] == 'A') ? 'specifyColorGreen' : (($answerSelect[5] == 'A') ? 'specifyColorRed' : (($answers[5] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-6-answers" value="A" <?php echo ($answerSelect[5] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[5] == 'A' & $answers[5] == 'A') ? 'specifyColorGreen' : (($answerSelect[5] == 'A') ? 'specifyColorRed' : (($answers[5] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-6-answers-A">A) Định lý Tallet </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[5] == 'B' & $answers[5] == 'B') ? 'specifyColorGreen' : (($answerSelect[5] == 'B') ? 'specifyColorRed' : (($answers[5] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-6-answers" value="B" <?php echo ($answerSelect[5] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[5] == 'B' & $answers[5] == 'B') ? 'specifyColorGreen' : (($answerSelect[5] == 'B') ? 'specifyColorRed' : (($answers[5] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-6-answers-B">B) Định lý Độ lệch chuẩn </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[5] == 'C' & $answers[5] == 'C') ? 'specifyColorGreen' : (($answerSelect[5] == 'C') ? 'specifyColorRed' : (($answers[5] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-6-answers" value="C" <?php echo ($answerSelect[5] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[5] == 'C' & $answers[5] == 'C') ? 'specifyColorGreen' : (($answerSelect[5] == 'C') ? 'specifyColorRed' : (($answers[5] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-6-answers-C">C) Định lý Pytago </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[5] == 'D' & $answers[5] == 'D') ? 'specifyColorGreen' : (($answerSelect[5] == 'D') ? 'specifyColorRed' : (($answers[5] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-6-answers" value="D" <?php echo ($answerSelect[5] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[5] == 'D' & $answers[5] == 'D') ? 'specifyColorGreen' : (($answerSelect[5] == 'D') ? 'specifyColorRed' : (($answers[5] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-6-answers-D">D) Định lý Newton </label>
            </div>

        </li>

        <li>

            <h3>Câu 7: "Đoàn quân việt nam đi chung lòng cứu quốc ..."" là lời của bài hát nào sau đây</h3>

            <div>
                <input id="<?php echo ($answerSelect[6] == 'A' & $answers[6] == 'A') ? 'specifyColorGreen' : (($answerSelect[6] == 'A') ? 'specifyColorRed' : (($answers[6] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-7-answers" value="A" <?php echo ($answerSelect[6] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[6] == 'A' & $answers[6] == 'A') ? 'specifyColorGreen' : (($answerSelect[6] == 'A') ? 'specifyColorRed' : (($answers[6] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-7-answers-A">A) Hà Nội của tôi </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[6] == 'B' & $answers[6] == 'B') ? 'specifyColorGreen' : (($answerSelect[6] == 'B') ? 'specifyColorRed' : (($answers[6] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-7-answers" value="B" <?php echo ($answerSelect[6] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[6] == 'B' & $answers[6] == 'B') ? 'specifyColorGreen' : (($answerSelect[6] == 'B') ? 'specifyColorRed' : (($answers[6] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-7-answers-B">B) thành phố Hồ Chí Minh muôn năm </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[6] == 'C' & $answers[6] == 'C') ? 'specifyColorGreen' : (($answerSelect[6] == 'C') ? 'specifyColorRed' : (($answers[6] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-7-answers" value="C" <?php echo ($answerSelect[6] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[6] == 'C' & $answers[6] == 'C') ? 'specifyColorGreen' : (($answerSelect[6] == 'C') ? 'specifyColorRed' : (($answers[6] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-7-answers-C">C) Sức mạnh tình yêu nước </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[6] == 'D' & $answers[6] == 'D') ? 'specifyColorGreen' : (($answerSelect[6] == 'D') ? 'specifyColorRed' : (($answers[6] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-7-answers" value="D" <?php echo ($answerSelect[6] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[6] == 'D' & $answers[6] == 'D') ? 'specifyColorGreen' : (($answerSelect[6] == 'D') ? 'specifyColorRed' : (($answers[6] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-7-answers-D">D) Tiến quân ca </label>
            </div>

        </li>

        <li>

            <h3>Câu 8: Điền vào chỗ trống "Đi một ngày đàng học ... sàng khôn" </h3>

            <div>
                <input id="<?php echo ($answerSelect[7] == 'A' & $answers[7] == 'A') ? 'specifyColorGreen' : (($answerSelect[7] == 'A') ? 'specifyColorRed' : (($answers[7] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-8-answers" value="A" <?php echo ($answerSelect[7] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[7] == 'A' & $answers[7] == 'A') ? 'specifyColorGreen' : (($answerSelect[7] == 'A') ? 'specifyColorRed' : (($answers[7] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-8-answers-A">A) mười</label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[7] == 'B' & $answers[7] == 'B') ? 'specifyColorGreen' : (($answerSelect[7] == 'B') ? 'specifyColorRed' : (($answers[7] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-8-answers" value="B" <?php echo ($answerSelect[7] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[7] == 'B' & $answers[7] == 'B') ? 'specifyColorGreen' : (($answerSelect[7] == 'B') ? 'specifyColorRed' : (($answers[7] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-8-answers-B">B) rổ </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[7] == 'C' & $answers[7] == 'C') ? 'specifyColorGreen' : (($answerSelect[7] == 'C') ? 'specifyColorRed' : (($answers[7] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-8-answers" value="C" <?php echo ($answerSelect[7] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[7] == 'C' & $answers[7] == 'C') ? 'specifyColorGreen' : (($answerSelect[7] == 'C') ? 'specifyColorRed' : (($answers[7] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-8-answers-C">C) một </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[7] == 'D' & $answers[7] == 'D') ? 'specifyColorGreen' : (($answerSelect[7] == 'D') ? 'specifyColorRed' : (($answers[7] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-8-answers" value="D" <?php echo ($answerSelect[7] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[7] == 'D' & $answers[7] == 'D') ? 'specifyColorGreen' : (($answerSelect[7] == 'D') ? 'specifyColorRed' : (($answers[7] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-8-answers-D">D) ít </label>
            </div>

        </li>

        <li>

            <h3>Câu 9: Khi không ăn một thời gian dài ta sẽ cảm thấy gì ?</h3>

            <div>
                <input id="<?php echo ($answerSelect[8] == 'A' & $answers[8] == 'A') ? 'specifyColorGreen' : (($answerSelect[8] == 'A') ? 'specifyColorRed' : (($answers[8] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-9-answers" value="A" <?php echo ($answerSelect[8] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[8] == 'A' & $answers[8] == 'A') ? 'specifyColorGreen' : (($answerSelect[8] == 'A') ? 'specifyColorRed' : (($answers[8] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-9-answers-A">A) Khát Nước </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[8] == 'B' & $answers[8] == 'B') ? 'specifyColorGreen' : (($answerSelect[8] == 'B') ? 'specifyColorRed' : (($answers[8] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-9-answers" value="B" <?php echo ($answerSelect[8] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[8] == 'B' & $answers[8] == 'B') ? 'specifyColorGreen' : (($answerSelect[8] == 'B') ? 'specifyColorRed' : (($answers[8] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-9-answers-B">B) Buồn Ngủ </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[8] == 'C' & $answers[8] == 'C') ? 'specifyColorGreen' : (($answerSelect[8] == 'C') ? 'specifyColorRed' : (($answers[8] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-9-answers" value="C" <?php echo ($answerSelect[8] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[8] == 'C' & $answers[8] == 'C') ? 'specifyColorGreen' : (($answerSelect[8] == 'C') ? 'specifyColorRed' : (($answers[8] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-9-answers-C">C) Đói </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[8] == 'D' & $answers[8] == 'D') ? 'specifyColorGreen' : (($answerSelect[8] == 'D') ? 'specifyColorRed' : (($answers[8] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-9-answers" value="D" <?php echo ($answerSelect[8] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[8] == 'D' & $answers[8] == 'D') ? 'specifyColorGreen' : (($answerSelect[8] == 'D') ? 'specifyColorRed' : (($answers[8] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-9-answers-D">D) Vui Vẻ </label>
            </div>

        </li>

        <li>

            <h3>Câu 10: Lịch nào dài nhất thế giới ? </h3>

            <div>
                <input id="<?php echo ($answerSelect[9] == 'A' & $answers[9] == 'A') ? 'specifyColorGreen' : (($answerSelect[9] == 'A') ? 'specifyColorRed' : (($answers[9] == 'A') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-10-answers" " value=" A" <?php echo ($answerSelect[9] == 'A') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[9] == 'A' & $answers[9] == 'A') ? 'specifyColorGreen' : (($answerSelect[9] == 'A') ? 'specifyColorRed' : (($answers[9] == 'A') ? "specifyColorGreen" : "")) ?>" for="question-10-answers-A">A) Lịch Maya </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[9] == 'B' & $answers[9] == 'B') ? 'specifyColorGreen' : (($answerSelect[9] == 'B') ? 'specifyColorRed' : (($answers[9] == 'B') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-10-answers" " value=" B" <?php echo ($answerSelect[9] == 'B') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[9] == 'B' & $answers[9] == 'B') ? 'specifyColorGreen' : (($answerSelect[9] == 'B') ? 'specifyColorRed' : (($answers[9] == 'B') ? "specifyColorGreen" : "")) ?>" for="question-10-answers-B">B) Lịch Sử </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[9] == 'C' & $answers[9] == 'C') ? 'specifyColorGreen' : (($answerSelect[9] == 'C') ? 'specifyColorRed' : (($answers[9] == 'C') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-10-answers" " value=" C" <?php echo ($answerSelect[9] == 'C') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[9] == 'C' & $answers[9] == 'C') ? 'specifyColorGreen' : (($answerSelect[9] == 'C') ? 'specifyColorRed' : (($answers[9] == 'C') ? "specifyColorGreen" : "")) ?>" for="question-10-answers-C">C) Lịch Làm Việc </label>
            </div>

            <div>
                <input id="<?php echo ($answerSelect[9] == 'D' & $answers[9] == 'D') ? 'specifyColorGreen' : (($answerSelect[9] == 'D') ? 'specifyColorRed' : (($answers[9] == 'D') ? "specifyColorGreen" : "")) ?>" type="radio" name="question-10-answers" " value=" D" <?php echo ($answerSelect[9] == 'D') ? 'checked' : '' ?> />
                <label id="<?php echo ($answerSelect[9] == 'D' & $answers[9] == 'D') ? 'specifyColorGreen' : (($answerSelect[9] == 'D') ? 'specifyColorRed' : (($answers[9] == 'D') ? "specifyColorGreen" : "")) ?>" for="question-10-answers-D">D) Lịch hẹn hò </label>
            </div>

        </li>

    </ol>

</body>

</html>